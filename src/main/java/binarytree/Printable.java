package binarytree;

@FunctionalInterface
public interface Printable {
    void print();
}
